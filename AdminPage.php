<!doctype html>
<html lang="en">
<head>
<body style="background-color:light;">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">





<nav class="navbar navbar-expand-lg navbar-light bg-info">
  
  <a class="navbar-brand" href="AdminPage.php">     <img src="/AdminWebClient/img/NHSsign.png" width="155" height="120" class="d-inline-block align-top" alt="">   </a>
  
  

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
      
      <li class="nav-item active text">
        <!-- used to create a gap for the logout button to be on the top right -->
    </ul>
    
    <form class="form-inline my-2 my-lg-0">
      
      <a class="btn btn-outline-dark my-2 my-sm-0" href="Login.php" role="button">Log out</a>
    </form>
  </div>
</nav>
  
 
  <div class ="button" align="center">
 
 <a type="button" class="btn btn-primary btn-lg active" href="SearchQuestions.php" aria-pressed="true">See Question Results</button>
 </a>
 <a type="button" href="CreatePatient.php" class="btn btn-primary btn-lg active"  aria-pressed="true">Create user</button>
 </a>
  </div>
		
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>