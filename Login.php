<?php
// Define variables and initialize with empty values

require_once 'api/dblogin.php';

// Processing form data when form is submitted
/*
if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $usernamewrong = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $passwordwrong = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate credentials
    if(empty($usernamewrong) && empty($passwordwrong)){
        // Prepare a select statement
        $conn = new mysqli($host, $user, $pass, $dbname);
        if($conn->connect_error) die($conn->connect_error);
        $sql = "SELECT UserID, UserName, UserPassword FROM user WHERE UserName = ?";
        
        if($stmt = mysqli_prepare($conn, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = $username;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $password, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            // Password is correct, so start a new session
                            session_start();
                            
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["UserID"] = $id;
                            $_SESSION["UserName"] = $username;
                            
                            // Redirect user to welcome page
                            header("AdminPage.php");
                        } else{
                            // Display an error message if password is not valid
                            $passwordwrong = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $usernamewrong = "No account found with that username.";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
            
            
        }
    }
    
    
    
}
*/if (isset($_POST['submit']))
{
    $usern = $_SESSION['username'];
    $pass = $_SESSION['password'];
    
    
    $query = "SELECT * FROM users WHERE userName = '".$usern."' AND pass = '".$pass."';";
    $qresult = mysqli_query($conn, $query);
    
    $num = mysqli_num_rows($qresult);
    
    if($num > 0) {
        header('location:AdminPage.php');
    }
    //Code keeps skipping to this line, need to find out why. Could re-write whole method to be more secure.
    else {
        header('location:Login.php');
        echo $query;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>

    <div class ="container">
      <div class="login-box">
      <div class="row">
        <h2>Login</h2>
        <p>Please fill in your credentials to login.</p>
        <form action="" method="POST">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control" required>
            </div>    
            <div class="form-group">
                <label>Password</label>
                <input type="text" name="password" class="form-control" required> 
            </div>            
                <button type="submit" class="btn btn-primary"> Login </button>
            </div>
        </form>
        </div>
        </div>  
</body>
</html>